Note
====

The mu-plugins directory will only look at php files directly inside it. Unlike the normal plugins directory, it will not look inside folders.

Add a require line to load.php to manually load plugins inside a sub direcrory.