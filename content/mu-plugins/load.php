<?php

// Register the theme directory
register_theme_directory( ABSPATH . 'wp-content/themes/' );

// Load MU Plugins located in sub directories
require WPMU_PLUGIN_DIR.'/vius-sandbox-anti-seo-plugin/vius-sandbox-anti-seo.php';