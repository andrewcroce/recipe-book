<?php
/*
This is a sample local-config.php file
In it, you *must* include the four main database defines

You may include other settings here that you only want enabled on your local development checkouts
*/

/*
Modify site URL and home URL to match the current server
*/
define( 'WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp' );
define( 'WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] );

/*
Modify database connection settings
*/

define( 'WP_LOCAL_DB', true ); // Use a local database.

if( WP_LOCAL_DB == true ) {

	define( 'DB_NAME', 'local_db_name' );
	define( 'DB_USER', 'local_db_user' );
	define( 'DB_PASSWORD', 'local_db_password' );
	define( 'DB_HOST', 'localhost' );

} else {
	
	// Note, your server must be configured to allow remote Mysql or this won't work.
	// In any case, remote Mysql is pretty slow, so using a local db is preferred.
	
	define( 'DB_NAME', 'remote_db_name' );
	define( 'DB_USER', 'remote_db_user' );
	define( 'DB_PASSWORD', 'remote_db_password' );
	define( 'DB_HOST', 'remote_db_host' );
	
}

